FROM mysql:5.7.36

COPY L2J_Server/sql/l2jdb_create.sql /docker-entrypoint-initdb.d/01-l2jdb_create.sql
COPY L2J_Server/sql/l2jdb_login_account.sql /docker-entrypoint-initdb.d/02-l2jdb_login_account.sql
COPY L2J_Server/sql/l2jdb_login_gameservers.sql /docker-entrypoint-initdb.d/03-l2jdb_login_gameservers.sql

ENV MYSQL_ROOT_PASSWORD='pwroot'
ENV MYSQL_DATABASE='l2jdb'
